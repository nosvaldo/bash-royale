var mazeCategories = [1,1,2,3,4,4,6,6];
var CardDiv = document.querySelectorAll( '.card' );
var CardsArray = [];
var TurnBtn = document.getElementById( 'turnBtn' );

var swiper = [];


var ScreenSaver = document.querySelectorAll( '.swiper' );

TurnBtn.addEventListener( 'click', startTurn );

document.addEventListener("DOMContentLoaded", function(event) {
    loadAllCardsCategories();

    
});

function loadAllCardsCategories(){
    for (let index = 0; index < 8; index++) 
        loadCardsCategory( index, mazeCategories[index] );

}

function loadCardsCategory( NumberCard, IdCategory ){
    CardsArray[ NumberCard ] = getCardsCategory( IdCategory );

    var sliderDiv = CardDiv[ NumberCard ].querySelector( '.swiper-wrapper' );

    shuffle( CardsArray[ NumberCard ] );

    CardsArray[ NumberCard ].forEach( c => {
        var swiperSlider = document.createElement( 'div' );
        //swiperSlider.setAttribute( 'id', 'ruleta-' + nombreRuleta );
        swiperSlider.setAttribute( 'class','swiper-slide' );

        var imgSlider = document.createElement( 'img' );
        imgSlider.src = c.ImgSrc;

        var spanDrop = document.createElement( 'span' );
        spanDrop.innerHTML = c.Elixir;
        spanDrop.setAttribute( 'class','card-drop' );

        swiperSlider.appendChild( imgSlider );
        swiperSlider.appendChild( spanDrop );
        sliderDiv.appendChild( swiperSlider );
    });


    swiper[NumberCard] = new Swiper('#card-' + ( NumberCard + 1) + ' .swiper', {
        loop: true,
        effect: 'cube',
        speed: 100,
        allowTouchMove: true,
        navigation: {
            enabled: false
        },
        scrollbar: {
            enabled: false
        }
    });
        
}

function getCardsCategory( IdCategory ){
    var cardsElements = []
    cardList.forEach(x=>{
        if( x.Categories.indexOf( IdCategory ) >= 0 )
            cardsElements[ cardsElements.length ] = x;
    });
    return cardsElements;
}


function startTurn(){
    disableScreenSaver();
    
    swiper.forEach(swip=>{
        var random =  Math.floor( Math.random() * (25 - 5) + 5 );
        console.log( random );
        swiperTurn( swip, random );
            
    });
}

function swiperTurn( swip, num ){
    swip.slideNext(100 + (10 * num));
    if( num > 0 )
        setTimeout( function(){ swiperTurn( swip, num-1 ) },100 + (10 * num));
}


function disableScreenSaver(){
    ScreenSaver.forEach( div=>{
        var divSaver = div.querySelector( '.swiper-wrapper-hidden' );

        if( divSaver !== null )
            divSaver.classList.remove( 'swiper-wrapper-hidden' );
    });

}


function shuffle(array) {
    let currentIndex = array.length,  randomIndex;
  
    // While there remain elements to shuffle.
    while (currentIndex > 0) {
  
      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
  
      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
  
    return array;
  }